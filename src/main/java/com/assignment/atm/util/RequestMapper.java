package com.assignment.atm.util;

import com.assignment.atm.model.CardDetails;
import com.assignment.atm.model.TransactionRequest;

public class RequestMapper {
    public static TransactionRequest getTransactionRequest(CardDetails cardDetails, int amount){
        return new TransactionRequest(cardDetails, amount);
    }
}
