package com.assignment.atm.util;

import com.assignment.atm.model.Notes;

public class AmountExtractor {

    public static int extractAmount(Notes notes){
        int totalAmountToBeAdded = 0;
        if(notes.getNote10()!=0){
            totalAmountToBeAdded += (10 * notes.getNote10());
        }
        if(notes.getNote50()!=0){
            totalAmountToBeAdded += (50 * notes.getNote50());
        }
        if(notes.getNote100()!=0){
            totalAmountToBeAdded += (100 * notes.getNote100());
        }
        if(notes.getNote200()!=0){
            totalAmountToBeAdded += (200 * notes.getNote200());
        }
        if(notes.getNote500()!=0){
            totalAmountToBeAdded += (500 * notes.getNote500());
        }
        if(notes.getNote2000()!=0){
            totalAmountToBeAdded += (2000 * notes.getNote2000());
        }
        return totalAmountToBeAdded;
    }

    public static int noteCounter(Notes notes){
        int totalNotes = 0;
        if(notes.getNote10()!=0){
            totalNotes += notes.getNote10();
        }
        if(notes.getNote50()!=0){
            totalNotes += notes.getNote50();
        }
        if(notes.getNote100()!=0){
            totalNotes += notes.getNote100();
        }
        if(notes.getNote200()!=0){
            totalNotes += notes.getNote200();
        }
        if(notes.getNote500()!=0){
            totalNotes += notes.getNote500();
        }
        if(notes.getNote2000()!=0){
            totalNotes += notes.getNote2000();
        }
        return totalNotes;
    }
}
