package com.assignment.atm.repo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class AtmRepo {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Value("${query.atm.limit.check}")
    private String LIMIT_CHECK_QUERY;

    @Value("${query.atm.update.balance}")
    private String UPDATE_BALANCE_QUERY;

    @Value("${query.atm.balance.check}")
    private String BALANCE_CHECK_QUERY;

    public String checkAtmBalance(){
        return jdbcTemplate.queryForObject(BALANCE_CHECK_QUERY,String.class);
    }

    public String checkAtmLimit(){
        return jdbcTemplate.queryForObject(LIMIT_CHECK_QUERY,String.class);
    }

    public void updateBalance(int newBalance){
        jdbcTemplate.update(UPDATE_BALANCE_QUERY,preparedStatement -> {
            preparedStatement.setInt(1, newBalance);
            preparedStatement.setString(2, "1");
        });
    }
}
