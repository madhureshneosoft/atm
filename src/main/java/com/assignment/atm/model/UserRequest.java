package com.assignment.atm.model;

import lombok.Data;

@Data
public class UserRequest {

    private CardDetails cardDetails;

    private String action;

    private Notes notes;

}
