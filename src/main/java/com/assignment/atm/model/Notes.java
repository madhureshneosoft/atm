package com.assignment.atm.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Notes {
    @JsonProperty("10")
    private int note10;

    @JsonProperty("50")
    private int note50;

    @JsonProperty("100")
    private int note100;

    @JsonProperty("200")
    private int note200;

    @JsonProperty("500")
    private int note500;

    @JsonProperty("2000")
    private int note2000;
}
