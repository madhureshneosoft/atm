package com.assignment.atm.controller;

import com.assignment.atm.exception.AtmException;
import com.assignment.atm.model.AxisResponse;
import com.assignment.atm.model.UserRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static com.assignment.atm.constant.Actions.*;

public class HelperMethods {

    public static boolean isRequestValid(UserRequest userRequest){
        List<String> errors = new ArrayList<>();
        if(!isActionValid(userRequest.getAction())){
            errors.add(INVALID_ACTION);
        }
        if(!isActive(userRequest.getCardDetails().getExpiryDate())){
            errors.add(CARD_EXPIRED);
        }

        if(errors.size()==0) {
            return true;
        } else {
            throw new AtmException(errors.toString());
        }
    }

    private static boolean isActionValid(String action){
        return getAllActions().contains(action);
    }

    private static boolean isActive(String expiryDate){
        return LocalDate.now().isBefore(LocalDate.parse(expiryDate));
    }

    public static ResponseEntity<AxisResponse> exceptionBuilder(String exception, long time){
        return new ResponseEntity<AxisResponse>(new AxisResponse(HttpStatus.BAD_REQUEST,exception,time),HttpStatus.BAD_REQUEST);
    }

    public static ResponseEntity<AxisResponse> responseBuilder(String response, long time){
        return new ResponseEntity<AxisResponse>(new AxisResponse(HttpStatus.OK,response,time),HttpStatus.OK);
    }
}
