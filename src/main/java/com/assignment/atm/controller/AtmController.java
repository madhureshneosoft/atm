package com.assignment.atm.controller;

import com.assignment.atm.exception.AtmException;
import com.assignment.atm.model.AxisResponse;
import com.assignment.atm.model.Notes;
import com.assignment.atm.service.AtmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/atm")
public class AtmController {

    @Autowired
    AtmService atmService;

    @RequestMapping("/")
    private String healthCheck(){
        return "Hey! i m healthy";
    }

    @RequestMapping("/atmBalance")
    private String checkAtmBalance(){
        return atmService.checkAtmBalance();
    }

    @PostMapping("/loadMoney")
    private ResponseEntity<AxisResponse> loadMoney(@RequestBody Notes notes){
        try {
            return HelperMethods.responseBuilder(atmService.loadMoney(notes),400);
        } catch (AtmException e){
            return HelperMethods.exceptionBuilder(e.getMessage(),400);
        }
    }

}
