package com.assignment.atm.controller;

import com.assignment.atm.model.AxisResponse;
import com.assignment.atm.model.UserRequest;
import com.assignment.atm.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserService userService;

    @Autowired
    RestTemplate restTemplate;

    @RequestMapping("/")
    private String healthCheck() {
        return "Hey! i m healthy";
    }

    @PostMapping("/validate")
    private ResponseEntity<AxisResponse> validate(@RequestBody UserRequest userRequest) {
        long start = System.currentTimeMillis();
        long end;
        try{
            end = System.currentTimeMillis()-start;
        return HelperMethods.responseBuilder(userService.isAuthenticated(userRequest.getCardDetails()).getResponseBody(),end);
        } catch (HttpClientErrorException e){
            end = System.currentTimeMillis()-start;
            return HelperMethods.exceptionBuilder(e.getResponseBodyAsString(),end);
        }
    }

    @PostMapping("/transaction")
    private ResponseEntity<AxisResponse> userAction(@RequestBody UserRequest userRequest) {
        long start = System.currentTimeMillis();
        long end;
        if (HelperMethods.isRequestValid(userRequest)) {
            end = System.currentTimeMillis()-start;
            return HelperMethods.responseBuilder(userService.actionResolver(userRequest).getResponseBody(),end);
        } else {
            end = System.currentTimeMillis()-start;
            return HelperMethods.exceptionBuilder("Invalid Request",end);
        }
    }
}
