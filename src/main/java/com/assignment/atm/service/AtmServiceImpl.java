package com.assignment.atm.service;

import com.assignment.atm.exception.AtmException;
import com.assignment.atm.model.Notes;
import com.assignment.atm.repo.AtmRepo;
import com.assignment.atm.util.AmountExtractor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AtmServiceImpl implements AtmService{

    @Autowired
    AtmRepo atmRepo;

    @Override
    public String checkAtmBalance() {
        return atmRepo.checkAtmBalance();
    }

    @Override
    public String loadMoney(Notes notes) {
        int limit = Integer.parseInt(atmRepo.checkAtmLimit());
        int balance = Integer.parseInt(atmRepo.checkAtmBalance());
        int totalAmountToBeAdded = AmountExtractor.extractAmount(notes);
        if(totalAmountToBeAdded+balance>limit){
            throw new AtmException("AMOUNT IS GREATER THAN LIMIT");
        } else {
            atmRepo.updateBalance(totalAmountToBeAdded+balance);
            return "Successful";
        }
    }
}
