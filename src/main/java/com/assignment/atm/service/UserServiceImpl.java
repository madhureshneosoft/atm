package com.assignment.atm.service;

import com.assignment.atm.exception.AtmException;
import com.assignment.atm.model.AxisResponse;
import com.assignment.atm.model.CardDetails;
import com.assignment.atm.model.UserRequest;
import com.assignment.atm.repo.AtmRepo;
import com.assignment.atm.util.AmountExtractor;
import com.assignment.atm.util.RequestMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import static com.assignment.atm.constant.Actions.*;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    AtmRepo atmRepo;

    @Autowired
    RestTemplate restTemplate;

    @Override
    public AxisResponse isAuthenticated(CardDetails cardDetails) {
        return restTemplate.postForObject("http://localhost:8081/axis/validate", cardDetails, AxisResponse.class);
    }

    @Override
    public AxisResponse actionResolver(UserRequest userRequest) {
        switch (userRequest.getAction()) {
            case WITHDRAW: {
                if (AmountExtractor.noteCounter(userRequest.getNotes()) <= 40) {
                    return withdraw(userRequest.getCardDetails(), AmountExtractor.extractAmount(userRequest.getNotes()));
                }
                throw new AtmException("Cannot withdraw more than 40 notes");
            }
            case DEPOSIT:
                return deposit(userRequest.getCardDetails(), AmountExtractor.extractAmount(userRequest.getNotes()));
            case CHECK_BALANCE:
                return checkBalance(userRequest.getCardDetails());
            default:
                throw new AtmException("Unexpected value: " + userRequest.getAction());
        }
    }


    private AxisResponse checkBalance(CardDetails cardDetails) {
        return restTemplate.postForObject("http://localhost:8081/axis/checkBalance", cardDetails, AxisResponse.class);
    }

    private AxisResponse withdraw(CardDetails cardDetails, int amount) {
        int atmBalance = Integer.parseInt(atmRepo.checkAtmBalance());
        if (amount <= atmBalance) {
            AxisResponse response = restTemplate.postForObject("http://localhost:8081/axis/withdraw", RequestMapper.getTransactionRequest(cardDetails, amount), AxisResponse.class);
            HttpStatus responseStatus = response.getResponseStatus();
            if (responseStatus.equals(HttpStatus.OK)) {
                atmRepo.updateBalance(atmBalance - amount);
            }
            return response;
        }
        throw new AtmException("Not enough balance in ATM");
    }

    private AxisResponse deposit(CardDetails cardDetails, int amount) {
        int balance = Integer.parseInt(atmRepo.checkAtmBalance());
        int atmLimit = Integer.parseInt(atmRepo.checkAtmLimit());
        if (balance + amount <= atmLimit) {
            AxisResponse response = restTemplate.postForObject("http://localhost:8081/axis/deposit", RequestMapper.getTransactionRequest(cardDetails, amount), AxisResponse.class);
            HttpStatus responseStatus = response.getResponseStatus();
            if (responseStatus.equals(HttpStatus.OK)) {
                atmRepo.updateBalance(balance + amount);
            }
            return response;
        } else {
            throw new AtmException("Exceeds ATM balance limit");
        }
    }

}
