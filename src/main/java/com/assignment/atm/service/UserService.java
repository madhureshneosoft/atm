package com.assignment.atm.service;

import com.assignment.atm.model.AxisResponse;
import com.assignment.atm.model.CardDetails;
import com.assignment.atm.model.UserRequest;

public interface UserService {
    public AxisResponse isAuthenticated(CardDetails cardDetails);
    public AxisResponse actionResolver(UserRequest userRequest);
}
