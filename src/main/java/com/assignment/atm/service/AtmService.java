package com.assignment.atm.service;

import com.assignment.atm.model.Notes;
import org.springframework.stereotype.Service;

@Service
public interface AtmService {
    public String checkAtmBalance();
    public String loadMoney(Notes notes);
}
