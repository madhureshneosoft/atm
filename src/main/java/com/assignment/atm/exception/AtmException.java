package com.assignment.atm.exception;

public class AtmException extends RuntimeException {

    public AtmException(String exception){
        super(exception);
    }
}
