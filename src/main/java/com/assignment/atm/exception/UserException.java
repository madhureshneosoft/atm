package com.assignment.atm.exception;

public class UserException extends RuntimeException {
    public UserException(String exception) {
        super(exception);
    }
}
